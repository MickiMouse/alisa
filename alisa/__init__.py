__version__ = '0.1.0'

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from config import Config

db = SQLAlchemy()
migrate = Migrate()


def create_app(config=Config):
    app = Flask(__name__)
    app.config.from_object(obj=config)

    db.init_app(app)
    migrate.init_app(app, db)
    return app
